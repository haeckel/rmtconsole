﻿using System.Text.RegularExpressions;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RMTConsole;

namespace RMTConsole.Tests
{
    [TestClass()]
    public class UnitTest1
    {
        [TestMethod()]
        public void NowTest()
        {
            string now = G.Now();
            Console.WriteLine(now);
            Regex.IsMatch(now,
                @"\d\d\d\d-\d\d\d\d-\d\d\d\d\d\d\d\d");
        }

        [TestMethod]
        public void TestMethod1()
        {
            string foo = "026D0200010375";
            var data = new ReceivedData(foo);
            Assert.IsTrue(data.type == ReceivedData.Type.COMMON);
        }
    }
}

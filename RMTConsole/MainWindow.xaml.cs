﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Collections.Generic;

namespace RMTConsole
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        ViewModel _vm;
        SettingPanel _settingPanel;

        public MainWindow()
        {
            InitializeComponent();
            G.w = this;
            G._mainThreadId = System.Threading.Thread.CurrentThread.ManagedThreadId;
            _vm = new ViewModel();

            SetFocusMoveOnEnter();
            LoadTable();
            comStat.SetBinding(BackgroundProperty, new Binding("serialStat") {
                Converter = new ComStatConverter(),
                ConverterParameter = comStat
            });
            DataContext = _vm;
        }
        private void SetFocusMoveOnEnter()
        {
            this.KeyDown += (sender, e) =>
            {
                if (e.Key != Key.Enter) { return; }
                var direction = Keyboard.Modifiers == ModifierKeys.Shift ? FocusNavigationDirection.Previous : FocusNavigationDirection.Next;
                (FocusManager.GetFocusedElement(this) as FrameworkElement)?.MoveFocus(new TraversalRequest(direction));
            };
        }
        private void LoadTable()
        {
            if (_vm.table == null) return;
            TableConverter mcon = new TableConverter();
            Brush blue1 = G.SolidBrush(0x8080ff);
            Brush blue2 = G.SolidBrush(0xC0C0FF);
            int tabIdx = 1000;
            int[] row = { 0, 0 };
            foreach (var set in _vm.table)
            {
                bool isJet = set.mode == "J";
                var t = isJet ? JetModeTable : RocketModeTable;
                int tidx = isJet ? 0 : 1;
                ++row[tidx];
                t.RowDefinitions.Add(new RowDefinition());
                var _0 = new Label
                {
                    Content = set.name,
                    Background = blue1,
                    BorderBrush = G.SilverBrush,
                    BorderThickness = new Thickness(0, 1, 1, 0),
                    VerticalContentAlignment = VerticalAlignment.Center,
                };
                Grid.SetColumn(_0, 0);
                Grid.SetRow(_0, row[tidx]);
                t.Children.Add(_0);
                for (int i = 0; i < set.data_cnt; ++i)
                {
                    var _1 = new Label
                    {
                        Content = i == 0 ? set.field1 : set.field2,
                        Background = blue2,
                        BorderBrush = G.SilverBrush,
                        BorderThickness = new Thickness(0, 1, 1, 0),
                        VerticalContentAlignment = VerticalAlignment.Center,
                    };
                    Grid.SetColumn(_1, 1 + 3 * i);
                    Grid.SetRow(_1, row[tidx]);
                    t.Children.Add(_1);
                    var _2 = new TextBox
                    {
                        VerticalAlignment = VerticalAlignment.Stretch,
                        HorizontalAlignment = HorizontalAlignment.Stretch,
                        VerticalContentAlignment = VerticalAlignment.Center,
                        HorizontalContentAlignment = HorizontalAlignment.Right,
                        BorderBrush = G.SilverBrush,
                        BorderThickness = new Thickness(0, 1, 1, 0),
                        TabIndex = tabIdx++,
                    };
                    _2.SetBinding(TextBox.TextProperty, new Binding(string.Format("table[{0}].data{1}", set.id, 1 + 2 * i))
                    {
                        Converter = mcon,
                        ConverterParameter = new ConvertParam( 1 + 2 * i, _2, set)
                    });
                    _2.GotFocus += new RoutedEventHandler((s, e) =>
                    {
                        ((TextBox)s).Text = "";
                    });
                    Grid.SetColumn(_2, 2 + 3 * i);
                    Grid.SetRow(_2, row[tidx]);
                    t.Children.Add(_2);
                    var _3 = new TextBox
                    {
                        VerticalAlignment = VerticalAlignment.Stretch,
                        HorizontalAlignment = HorizontalAlignment.Stretch,
                        VerticalContentAlignment = VerticalAlignment.Center,
                        HorizontalContentAlignment = HorizontalAlignment.Right,
                        Background = G.SilverBrush,
                        BorderBrush = G.SilverBrush,
                        BorderThickness = new Thickness(0, 1, 1, 0),
                        IsReadOnly = true,
                    };
                    _3.SetBinding(TextBox.TextProperty, new Binding(string.Format("table[{0}].data{1}", set.id, 2 + 2 * i))
                    {
                        Converter = mcon,
                        ConverterParameter = new ConvertParam(2 + 2 * i, _3, set)
                    });
                    Grid.SetColumn(_3, 3 + 3 * i);
                    Grid.SetRow(_3, row[tidx]);
                    t.Children.Add(_3);
                }
            }
        }
        private void InitializeWindow(object sender, RoutedEventArgs e)
        {
            //T1_S.DataContext = new int[] {111,222,333};
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            _vm.com.SwitchOpenClose();
        }

        private void button_Copy_Click(object sender, RoutedEventArgs e)
        {
            //_com.Write();
        }

        private void OnCloseWindow(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //e.Cancel = MessageBox.Show("終了しますか?", "確認", MessageBoxButton.YesNo) == MessageBoxResult.No;
        }

        class TestData
        {
            public static ViewModel vm;
            private byte[] bytes;
            public TestData(char command)
            {
                var upper = G.Upper(command);
                var set = Array.Find(vm.table, (set_) => set_.command == upper);
                if (set == null)
                {
                    G.Log("Invalid Command: " + command);
                    return;
                }
          
                bytes = new byte[set.data_cnt*2+5];
                bytes[0] = 2;
                bytes[1] = (byte)command;
                bytes[2] = (byte)(set.data_cnt * 2);
                for(int i = 0; i < set.data_cnt; ++i)
                {
                    bytes[3 + 2*i] = (byte)(1+3*i);
                    bytes[4 + 2*i] = (byte)(1+3*i);
                }
                bytes[bytes.Length-2] = 3;
                bytes[bytes.Length - 1] = G.CheckSum(bytes);
            }
            public string str { get => G.Bytes2String(bytes); }
        }
        private void OnTest(object sender, RoutedEventArgs e)
        {
            //TestData.vm = _vm;
            //var t = new TestData('a');
            //var command = t.str;
            //_vm.com.debugEnque(command);
            _vm.com.debugEnque("020600030B");
        }

        private void ClearLog(object sender, RoutedEventArgs e)
        {
            LogWindow.Items.Clear();
        }

        private void ShowConnectionSettingPanel(object sender, RoutedEventArgs e)
        {
            _settingPanel= new SettingPanel();
            _settingPanel.Show();
        }

        private void ClearReceivedData(object sender, RoutedEventArgs e)
        {
            var t = _vm.table;
            for (int i = 0; i < t.Length; ++i)
            {
                t[i].data2 = -1;
                t[i].data4 = -1;
            }
        }

        private void SendParameters(object sender, RoutedEventArgs e)
        {
            _vm.SendParameters();
        }

        private void SaveParameters(object sender, RoutedEventArgs e)
        {
            _vm.SaveTable();
        }

    }
}

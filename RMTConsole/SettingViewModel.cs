﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.IO.Ports;
using System.Management;

namespace RMTConsole
{
    class SettingViewModel
    {
        public string[] PortList { get; } = SerialPort.GetPortNames();
        public int[] BaudRateList { get; } = 
        {
            110,300,600,1200,2400,4800,9600,14400,19200,38400,57600,115200,230400,460800
        };
        public Object[] ParityList { get; } =
        {
            new {val=Parity.None,name="none" },
            new {val=Parity.Odd,name="odd" },
            new {val=Parity.Even,name="even" },
            new {val=Parity.Mark,name="mark" },
            new {val=Parity.Space,name="space" }
        };
        public Object[] DataBitList { get; } =
        {
            new {val=7,name="7bit"},
            new {val=8,name="8bit"},
        };
        public Object[] StopBitList { get; } =
        {
            new {val=StopBits.None,name="none"},
            new {val=StopBits.One,name="1 bit"},
            new {val=StopBits.OnePointFive,name="1.5 bit"},
            new {val=StopBits.Two,name="2 bit"},
        };
        public Object[] NewLineList { get; } =
        {
            new {val='\n',name="LF" },
            new {val='\r',name="CR" },
        };
        public int ReadTimeOut { get; set; } = 1000;
        public int WriteTimeOut { get; set; } = 1000;

        public SettingViewModel()
        {
            
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media;
namespace RMTConsole
{
    public class DataSet : INotifyPropertyChanged
    {
        public int id { get; set; }
        public string mode { get; set; }
        public string name { get; set; }
        public int data_cnt { get; set; }
        public bool x10 { get; set; }
        public char command { get; set; }
        public string field1 { get; set; }
        public string field2 { get; set; }
        private int data1_, data2_, data3_, data4_;
        public int data1 { get => data1_; set { data1_ = value; Notify(); } }
        public int data2 { get => data2_; set { data2_ = value; Notify(); } }
        public int data3 { get => data3_; set { data3_ = value; Notify(); } }
        public int data4 { get => data4_; set { data4_ = value; Notify(); } }
        public event PropertyChangedEventHandler PropertyChanged;
        private void Notify([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public override string ToString()
        {
            byte[] b = new byte[data_cnt*2+5];
            b[0] = 2;
            b[1] = (byte)command;
            b[2] = (byte)(data_cnt * 2);
            for(int i = 0; i < data_cnt; ++i)
            {
                var data = i == 0 ? data1_ : data3_;
                b[3 + 2 * i] = (byte)(data >> 8);
                b[4 + 2 * i] = (byte)data;
            }
            b[b.Length - 2] = 3;
            b[b.Length - 1] = G.CheckSum(b);
            return G.Bytes2String(b);
        }
    }
    public class ReceivedData
    {
        public enum Type
        {
            ILLEGAL,
            ACK,
            NACK,
            COMMON
        }
        public Type type = Type.ILLEGAL;
        public char command;
        public ushort[] data;

        public ReceivedData(string received_data)
        {
            if (received_data.Length == 0) return;
            if (received_data.Length % 2 > 0) return;
            var bytes = new byte[received_data.Length / 2];
            for (int i = 0; i < bytes.Length; ++i)
            {
                string b2 = received_data.Substring(2 * i, 2);
                try
                {
                    bytes[i] = Byte.Parse(b2, NumberStyles.HexNumber);
                }
                catch
                {
                    return;
                }
            }
            Parse(ref bytes);
        }
        public ReceivedData(ref byte[] bytes)
        {
            Parse(ref bytes);
        }
        private void Parse(ref byte[] bytes)
        {
            if (bytes[0] != 2) return;
            if (bytes[2] != bytes.Length - 5) return;
            byte sum = 0;
            for (int i = 0; i < bytes.Length - 1; ++i)
            {
                sum += bytes[i];
            }
            if (sum != bytes[bytes.Length - 1]) return;
            command = (char)bytes[1];
            data = new ushort[bytes[2] / 2];
            for (int i = 0; i < data.Length; ++i)
            {
                data[i] = (ushort)(bytes[3 + 2 * i] << 8 | bytes[4 + 2 * i]);
            }
            type = command == 0x6 ? Type.ACK :
                    command == 0x15 ? Type.NACK :
                    Type.COMMON;
        }
    }
}

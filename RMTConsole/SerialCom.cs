﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace RMTConsole
{
    class SerialCom
    {
        ViewModel vm;
        const string ACK = "020600030B";
        const string NACK = "021500031A";
        SerialPort _port;
        Object _lock = new object();
        private LinkedList<string> _listToSend = new LinkedList<string>();
#if DEBUG
        private Queue<string> _debugReceiveQue = new Queue<string>();
        public void debugEnque(string command)
        {
            if (_port != null && _port.IsOpen ? false : !Open() ) return;
            lock(_lock)
                _debugReceiveQue.Enqueue(command);
        }
#endif
        public SerialCom(ViewModel vm_)
        {
            vm = vm_;
        }
        public bool Open()
        {
            if (_port != null && _port.IsOpen) return true;
            try
            {
                _port = new SerialPort(G.Setting.port, 38400, Parity.None, 8, StopBits.Two);
                _port.ReadTimeout = G.Setting.readTimeout;
                _port.WriteTimeout = 1000; 
                _port.NewLine = "\r";
                _port.Open();
            }
            catch
            {
                G.Log("接続に失敗しました。");
                G.Log(String.Format("Name:{0} BaudRate:{1} Parity:{2} DataBits:{3} StopBits:{4}",
                    _port.PortName,_port.BaudRate,_port.Parity,_port.DataBits,_port.StopBits));
                return false;
            }
            Task.Run(() => SerialIO());
            vm.serialStat = 1;
            return true;
        }
        public bool Close()
        {
            _port.Close();
            vm.serialStat = 0;
            return false;
        }
        public bool SwitchOpenClose()
        {
             return _port!=null&&_port.IsOpen ? Close() : Open(); 
        }
        private void SerialIO()
        {
            try
            {
                while (true)
                {
                    Receive();
                    Send();
                }
            }
            catch
            {
                _listToSend = new LinkedList<string>();
                _debugReceiveQue = new Queue<string>();
                G.Log("ポートを閉じました");
            }
        }
        private void Send()
        {
            try
            {
                lock (_lock)
                {
                    if (_listToSend.Count > 0)
                    {
                        string command = _listToSend.First.Value;
                        G.Log("送信: " + command);
                        _port.WriteLine(command);
                    }
                }
            }
            catch (TimeoutException) { }
        }
        private void Receive()
        {
            string buf = "";
            try
            {
                buf = "";
#if DEBUG
                lock (_lock)
                {
                    if (_debugReceiveQue.Count > 0)
                    {
                        buf = _debugReceiveQue.Dequeue();
                    }
                }
                if(buf=="") buf = _port.ReadLine();
#else
                _port.ReadLine();
#endif
            }
            catch (TimeoutException)
            {
                vm.serialStat = 1;
                lock (_lock)
                {
                    if (_listToSend.Count > 0 && _listToSend.First.Value == ACK)
                    {
                        RemoveFirst();
                    }
                }
                return;
            }

            G.Log("受信: " + buf);
            G.Log(G.DumpString(buf));
            vm.serialStat = 2;
            var data = new ReceivedData(buf);
            switch(data.type)
            {
                case ReceivedData.Type.ILLEGAL:
                    G.Log("Received Illegal Data");
                    SetNack();
                    break;
                case ReceivedData.Type.ACK:
                    RemoveFirst();
                    break;
                case ReceivedData.Type.NACK:
                    break;
                default:
                    G.Log("残り: "+_listToSend.Count);
                    SetAck();
                    vm.SetReceivedData(ref data);
                    break;
            }
        }
        private void RemoveFirst()
        {
            lock (_lock)
                if(_listToSend.Count>0)
                    _listToSend.RemoveFirst();
        }
        public void Enque(string data)
        {
            lock (_lock)
                _listToSend.AddLast(data);
        }
        public void SetAck()
        {
            lock(_lock)
                _listToSend.AddFirst(ACK);
        }
        public void SetNack()
        {
            lock (_lock)
                _listToSend.AddFirst(NACK);
        }
    }
}

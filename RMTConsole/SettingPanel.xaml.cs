﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RMTConsole
{
    /// <summary>
    /// SettingView.xaml の相互作用ロジック
    /// </summary>
    public partial class SettingPanel : Window
    {
        SettingViewModel vm;
        public SettingPanel()
        {
            InitializeComponent();
            DataContext = vm = new SettingViewModel();
            PortBox.SelectedIndex = 2;// G.Setting.port;
            ReadTimeoutBox.Text = G.Setting.readTimeout.ToString();
            BaudRateBox.SelectedValue = 38400;
        }

        private void LoadSetting(object sender, RoutedEventArgs e)
        {
            vm.PortList[0] = "11111111111";
            PortBox.SelectedValue = "11111111111";
            Console.WriteLine(PortBox.SelectedValue);
        }

        private void ApplySetting(object sender, RoutedEventArgs e)
        {
            int.TryParse(ReadTimeoutBox.Text,out int readTimeout);
            string port = (string)PortBox.SelectedValue;
            G.Log(port+" "+readTimeout);
            G.Setting.port = port;
            G.Setting.readTimeout = readTimeout;
            this.Close();
        }
    }
}

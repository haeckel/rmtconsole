﻿using System;
using System.Linq;
using System.IO;
using System.ComponentModel;
using CsvHelper;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Collections.Generic;

namespace RMTConsole
{
    class ViewModel : INotifyPropertyChanged
    {
        private int serialStat_;
        public int serialStat { get => serialStat_; set { if (value != serialStat_) {serialStat_ = value; Notify(); } } }
        public DataSet[] table { get; set; }
        public SerialCom com;
        public ViewModel()
        {
            com = new SerialCom(this);
            LoadTable();
        }
        private void LoadTable()
        {
            var fname = @"data\SerialSpec.csv";
            try
            {
                using (var textReader = File.OpenText(fname))
                {
                    var csv = new CsvReader(textReader);
                    var records = csv.GetRecords<DataSet>();
                    table = records.ToArray();
                }
            }
            catch
            {
                G.Log("ファイルオープンに失敗しました。\n別プロセスで使用していないか確認して下さい。\n"+ Directory.GetCurrentDirectory() + @"\" + fname);
            }
        }
        public void SaveTable()
        {
            var fname = string.Format("data/{0}.csv", G.Now());
            try
            {
                using (var writer = new StreamWriter(fname))
                {
                    var csv = new CsvWriter(writer);
                    csv.WriteRecords(table);
                }
            }
            catch
            {
                MessageBox.Show("保存に失敗しました。\n"+fname,@"Error");
            }
        }

        public void SetReceivedData(ref ReceivedData data)
        {
            var command = G.Upper(data.command);
            DataSet dset =Array.Find(table, (DataSet set) => { return set.command == command; });
            if (dset == null) return;
            for(int i = 0; i < data.data.Length; ++i) {
                if (i == 0) dset.data2 = data.data[0];
                else dset.data4 = data.data[1];
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void Notify([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void SendParameters()
        {
            if (!com.Open()) return;
            foreach(DataSet set in table)
            {
                com.Enque(set.ToString());
            }
        }
    }
}

﻿using System;
using System.Windows.Media;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace RMTConsole
{
    static class G
    {
        public static Brush SilverBrush = SolidBrush(0xcccccc);
        public static MainWindow w;
        public static int _mainThreadId;
        public static DataSet[] map;
        public static class Setting
        {
            public static string port = "COM4";
            public static int readTimeout = 1000;
        }
        public static void Log(string msg)
        {
            w.Dispatcher.Invoke(new Action(() => {
                var items = w.LogWindow.Items;
                items.Add(new {Time=Now(),Description=msg });
                w.LogWindow.ScrollIntoView(items[items.Count-1]);
            }));
        }
        public static Brush SolidBrush(int RGB)
        {
            string rgb = string.Format("#{0,0:X6}",RGB);
            var converter = new BrushConverter();
            return (Brush)converter.ConvertFromString(rgb);
        }
        public static byte CheckSum(byte[] b)
        {
            byte ret = 0;
            for(int i = 0; i < b.Length-1; ++i)
            {
                ret += b[i];
            }
            return ret;
        }
        public static string Bytes2String(byte[] bytes)
        {
            return bytes==null ? "": BitConverter.ToString(bytes).Replace("-", string.Empty);
        }
        public static string DumpString(String buf)
        {
            return BitConverter.ToString(Encoding.UTF8.GetBytes(buf));
        }
        public static char Upper(char lower)
        {
            return (lower < 'a' ) ? lower: (char)(lower - 0x20);
        }
        public static string Now()
        {
            var time = DateTime.Now.ToLocalTime();
            int YYYY = time.Year;
            int MM = time.Month;
            int DD = time.Day;
            int hh = time.Hour;
            int mm = time.Minute;
            int ss = time.Second;
            return string.Format("{0}{1:00}{2:00}-{3:00}{4:00}{5:00}", YYYY, MM, DD, hh, mm, ss);
        }
    }
}

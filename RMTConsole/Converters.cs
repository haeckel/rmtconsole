﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Controls;
using System.Windows.Media;

namespace RMTConsole
{
    class ConvertParam
    {
        public int data_field;
        public TextBox me;
        public DataSet set;
        public ConvertParam(int data_field_,TextBox me_, DataSet set_)
        {
            data_field = data_field_;
            me = me_;
            set = set_;
        }
    }
    class TableConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var p = (ConvertParam)parameter;
            var v = System.Convert.ToInt32(value);

            bool isReceiveField = p.data_field % 2 == 0;
            if (v < 0)
            {
                if(isReceiveField) p.me.Background = G.SilverBrush;
                return "";
            }
            if (isReceiveField)
            {
                int other = p.data_field<3 ? p.set.data1 : p.set.data3;
                int color = v == other ? 0x00ff00 : 0xff00ff;
                p.me.Background = G.SolidBrush(color);
            }
            else
            {
                int pink = 0xffccff;
                int color = v>2000 ? pink : p.set.x10 && 0 < v && v < 4 ? pink : -1;
                p.me.Background = G.SolidBrush(color);
            }
            return p.set.x10 ? string.Format("{0:0.0}", (double)v / 10) : v.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var p = (ConvertParam)parameter;
            var v1 = System.Convert.ToString(value);
            int v2;
            if (v1 == "")
            {
                v2 = p.data_field < 3 ? p.set.data1 : p.set.data3;
            }
            else
            {
                bool success = double.TryParse(v1, out double v3);
                if (success)
                    v2 = p.set.x10 ? (int)(v3 * 10) : (int)v3;
                else
                    v2 = 0;
            }
            return v2 < 0 ? 0:v2;
        }
    }
    class ComStatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var stat = System.Convert.ToInt32(value);
            Brush[] brush =
            {
                G.SolidBrush(0xcccccc),
                G.SolidBrush(0xffff00),
                G.SolidBrush(0x00ff00)
            };
            string[] str =
            {
                "未接続", "待機中", "通信中"
            };
            ((Label)parameter).Content = str[stat];
            return brush[stat];
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
